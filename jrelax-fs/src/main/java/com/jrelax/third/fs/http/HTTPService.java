package com.jrelax.third.fs.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FTP服务
 * Created by zengchao on 2016-12-26.
 */
public class HTTPService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private static HTTPService instance;

    public static HTTPService getInstance() {
        if (instance == null) {
            instance = new HTTPService();
        }
        return instance;
    }

    private HTTPService() {

    }

    public void start() {
        logger.info("FS:HTTP服务启动");
    }

    public void stop() {
        logger.info("FS:HTTP服务已停止");
    }
}
